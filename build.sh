# build

echo "Build for java 11"
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk use java 11.0.15-zulu
sdk current java

mvn --define skipTests=true \
    --activate-profiles useItips \
    --settings settings.xml clean install  | tee mvn.build.log.txt
if [ $? -eq 0 ]
then
  echo "Success"
else
  echo "Problem"
  echo $?
  exit
fi

echo "Done build.sh"
