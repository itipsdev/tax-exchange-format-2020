
echo "Deploy"

pwd

source build.sh

mvn --version

mvn -e clean install deploy -DskipTests=true
if [ $? -eq 0 ]
then
  echo "Success"
else
  echo "Problem"
  echo $?
  exit
fi

echo "Done deploy.sh"
