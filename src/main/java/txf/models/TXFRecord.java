package txf.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.math.BigDecimal;

public class TXFRecord {

    private String code;

    private String copy;

    private String line;

    private String description;

    private BigDecimal amount;

    public TXFRecord( ) {
    }

    public String getCode( ) {
        return code;
    }

    public void setCode( String code ) {
        this.code = code;
    }

    public String getCopy( ) {
        return copy;
    }

    public void setCopy( String copy ) {
        this.copy = copy;
    }

    public String getLine( ) {
        return line;
    }

    public void setLine( String line ) {
        this.line = line;
    }

    public String getDescription( ) {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public BigDecimal getAmount( ) {
        return amount;
    }

    public void setAmount( BigDecimal amount ) {
        this.amount = amount;
    }

    public String toJson( ) {

        Gson gson = new GsonBuilder( ).setPrettyPrinting( ).create( );

        return gson.toJson( this );

    }

}


