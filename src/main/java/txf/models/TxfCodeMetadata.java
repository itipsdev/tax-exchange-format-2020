package txf.models;

import java.util.Comparator;

public class TxfCodeMetadata {

    // "L" - Level Of Indent.
    // Valid values are 0-2.
    // 0 represents a form or schedule name.
    // 1 represents a section on a form, and
    // 2 is an actual line that can be filled in.
    // Only refnums for level 2 items should be exported to tax software in the export file.
    private String level;

    // "RNum" - Refnum
    // Unique identifier for this tax line.
    // Valid values are 1-999.
    // This value  may not be reused in other years to mean different items.
    private String code;

    // "Name" - Name of category
    // The name the user will see when referring to this tax line item.
    // This field must be 30 characters or fewer, and must be enclosed in quotes.
    // The name of the form and section should not be included in this name as it is implied by the items with a lower indention level above.
    private String description;

    // "Cpy" - Copy
    // Valid values are Y and N.
    // If Y, multiple copies of this tax item are allowed.
    // This is useful for schedules which may be submitted with many copies (like Schedule C).
    private String multiCopy;

    // "Srt" - Sort
    // Valid values are N, A, C and P.
    // N indicates no sorting or subtotaling within the tax line is necessary.
    // A indicates that the items within the tax line should be sorted and subtotaled by the asset or liability associated with the transaction (the Quicken account).
    // C indicates a sorting and subtotaling by the income or expense associated with the transaction (the Quicken category).
    // P indicates sorting and subtotaling by the name associated with the transaction (the Quicken payee).
    private String sortCode;

    // "Sgn" - Sign
    // Valid values are I, E, B and S.
    // I indicates this is an income line and that the normal sign for an exported value for this tax line item is '+'.
    // E indicates this is an expense line and the normal sign is '-'.
    // B indicates either + or - is expected, and
    // S indicates a string value.
    private String signCode;

    // "Frm" - Record Format
    // Valid values are 0-6.
    // See format descriptions in Section VII for more info.
    private String format;

    public static final String FORMAT_0 = "0";
    public static final String FORMAT_1 = "1";
    public static final String FORMAT_2 = "2";
    public static final String FORMAT_3 = "3";
    public static final String FORMAT_4 = "4";
    public static final String FORMAT_5 = "5";
    public static final String FORMAT_6 = "6";

    // "Line" - IRS line location
    // The line indicates which year, schedule and line number on the schedule this tax item represents.
    // It is provided to uniquely describe the line number for the tax software companies and to indicate the last year this item was used or updated.
    private String taxYear;
    private String irsSchedule;
    private String lineNumber;

    public TxfCodeMetadata( ) {

    }

    public String getLevel( ) {
        return level;
    }

    public void setLevel( String level ) {
        this.level = level;
    }

    public String getCode( ) {
        return code;
    }

    public void setCode( String code ) {
        this.code = code;
    }

    public String getDescription( ) {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public String getMultiCopy( ) {
        return multiCopy;
    }

    public void setMultiCopy( String multiCopy ) {
        this.multiCopy = multiCopy;
    }

    public String getSortCode( ) {
        return sortCode;
    }

    public void setSortCode( String sortCode ) {
        this.sortCode = sortCode;
    }

    public String getSignCode( ) {
        return signCode;
    }

    public void setSignCode( String signCode ) {
        this.signCode = signCode;
    }

    public String getFormat( ) {
        return format;
    }

    public void setFormat( String format ) {
        this.format = format;
    }

    public String getTaxYear( ) {
        return taxYear;
    }

    public void setTaxYear( String taxYear ) {
        this.taxYear = taxYear;
    }

    public String getIrsSchedule( ) {
        return irsSchedule;
    }

    public void setIrsSchedule( String irsSchedule ) {
        this.irsSchedule = irsSchedule;
    }

    public String getLineNumber( ) {
        return lineNumber;
    }

    public void setLineNumber( String lineNumber ) {
        this.lineNumber = lineNumber;
    }

    public static final Comparator<TxfCodeMetadata> CODE_COMPARATOR = new Comparator<TxfCodeMetadata>( ) {

        public int compare(
            TxfCodeMetadata a1,
            TxfCodeMetadata a2
        ) {

            return a1.getCode( ).compareTo( a2.getCode( ) );

        }

    };

    public static final Comparator<TxfCodeMetadata> FORM_COMPARATOR = new Comparator<TxfCodeMetadata>( ) {

        public int compare(
            TxfCodeMetadata a1,
            TxfCodeMetadata a2
        ) {

            String schedule1 = a1.getIrsSchedule( );
            if ( schedule1 == null ) return 0;

            String schedule2 = a2.getIrsSchedule( );
            if ( schedule2 == null ) return 0;

            int compare1 = schedule1.compareTo( schedule2 );

            return compare1;

        }

    };

    public static final Comparator<TxfCodeMetadata> FORMAT_COMPARATOR = new Comparator<TxfCodeMetadata>( ) {

        public int compare(
            TxfCodeMetadata a1,
            TxfCodeMetadata a2
        ) {

            String format1 = a1.getFormat( );
            if ( format1 == null ) return 0;

            String format2 = a2.getFormat( );
            if ( format2 == null ) return 0;

            int compare1 = format1.compareTo( format2 );

            return compare1;

        }

    };

}
