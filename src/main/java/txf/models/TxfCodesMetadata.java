package txf.models;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;

import java.io.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@XmlRootElement(name = "txfCodesMetadata")
@XmlAccessorType( XmlAccessType.FIELD)
public class TxfCodesMetadata {

    private static Logger LOGGER
        = Logger.getLogger( TxfCodesMetadata.class.getSimpleName( ) );

    @XmlElementWrapper( name = "codesMetadata" )
    @XmlElement( name = "codeMetadata" )
    private List<TxfCodeMetadata> list;

    public TxfCodesMetadata( ) {

        this.list = new ArrayList<>(  );

    }

    public List<TxfCodeMetadata> getList( ) {
        return list;
    }

    public void setList( List<TxfCodeMetadata> list ) {
        this.list = list;
    }

    /**
     * Load data from resources xml file
     */
    public void load( ) {

        String fileName = "xml/TxfCodesMetadata.xml";

        ClassLoader classLoader = TxfCodesMetadata.class.getClassLoader( );

        final InputStream resourceAsStream = classLoader.getResourceAsStream( fileName );

        Reader reader = null;

        if ( resourceAsStream != null ) {

            reader = new InputStreamReader( resourceAsStream );

            TxfCodesMetadata other = TxfCodesMetadata.fromXmlReader( reader );

            this.setList( other.getList( ) );

        }

    }

    /**
     * Add
     * @param metadata Metadata
     */
    public void add( TxfCodeMetadata metadata ) {

        this.list.add( metadata );

    }

    /**
     * To xml
     *
     * @return Xml string
     */
    public String toXml( ) {

        StringWriter writer = new StringWriter();

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance( TxfCodesMetadata.class );

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // Output pretty printed
            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );

            jaxbMarshaller.marshal( this, writer );

        } catch ( JAXBException e ) {

            LOGGER.severe( e.getMessage( ) );

        }

        return writer.toString();

    }

    /**
     * From xml
     *
     * @param asXml String
     * @return TxfCodesMetadata instance
     */
    public static TxfCodesMetadata fromXml(
        String asXml
    ) {

        TxfCodesMetadata obj = new TxfCodesMetadata( );

        try {

            Reader reader = new StringReader( asXml );

            JAXBContext jaxbContext = JAXBContext.newInstance( TxfCodesMetadata.class );

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller( );

            obj = (TxfCodesMetadata) jaxbUnmarshaller.unmarshal( reader );

        } catch ( JAXBException e ) {

            LOGGER.severe( e.getMessage( ) );

        }

        return obj;

    }

    public static TxfCodesMetadata fromXmlReader(
        Reader reader
    ) {

        TxfCodesMetadata obj = new TxfCodesMetadata( );

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance( TxfCodesMetadata.class );

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller( );

            obj = (TxfCodesMetadata) jaxbUnmarshaller.unmarshal( reader );

        } catch ( JAXBException e ) {

            LOGGER.severe( e.getMessage( ) );

        }

        return obj;

    }

    public static void main( String[] args ) {

        TxfCodesMetadata txfCodesMetadata = new TxfCodesMetadata( );

        txfCodesMetadata.load( );

        System.out.println( txfCodesMetadata.getList( ).size( ) );

    }

}
