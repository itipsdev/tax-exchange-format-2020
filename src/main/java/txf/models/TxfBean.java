package txf.models;

import org.apache.commons.lang3.StringUtils;

import java.io.StringWriter;

import java.math.BigDecimal;

public class TxfBean {

    /*
     
    There are 7 record formats, labeled 0 through 6 with various
    additional fields.

    The fields for these record formats are:

    Record Format 0

      Note: used for boolean records such as "spouse" indicator

    Record Format 1 (placed before the 'X' detail line in the record)
    
      $ amount

    Record Format 2
    
      P Text value

      Note: String value used for "description" fields such as Schedule C Line A

    Record Format 3
      
      $ amount
      P description

      Note: used for fields such as interest where bank name/account#
            and $ amount required.

    Record Format 4
      
      P security
      D date acquired
      D date sold
      $ cost basis
      $ sales net (net of commission)

    Record Format 5
    
      P security
      D date acquired
      D date sold
      $ cost basis
      $ sales net
      $ Depreciation Allowed OR Disallowed wash sale amount (added for TY11)

      Note: expanded Format 5 to use for new TY11 cost basis reporting rules. Format 5 will
       be used with Taxrefs 321,323,673,682,711,712,713,714,715,716 when reporting a disallowed wash sale amount. 

    Record Format 6
    
      D date paid
      $ amount paid
      P state initials

      Note: used for quarterly federal and state estimated tax payments; 
            state initials ignored for federal estimates.

B. Codes and Definition of Fields

"V" Version
  Indicates which version of the tax line item maintenance file was used to
  export this file. The current number can be found at the top of this file.

"A" Accounting Program Name/Version
  Indicates which program (including version) exported this file.

"D" Export Date
  Date on which this file was exported.

"T" Type
  Either S for summary or D for detail.  Default value is S.

"N" Refnum
  The tax category refnum for this item.  There is no default value
  (it must be supplied).  This refnum should come from the tax line item
  maintenance file.

"C" Copy
  Integer between 1 and 255.  Default value is 1.  This is the copy number
  for multi-copy forms (such as Schedule C.)  If there is only one copy, the
  value should be 1.

"L" Line
  A positive integer.  The default value is 1.  This is the line number for
  multi-line items, such as interest income on Schedule B.  If there is only
  one line item, this number should be 1.

"X" Detail
  Extra text that can be used by the tax software, if desired.  The initial
  intent of this field is for use in a supporting statement.

"$" Amount
  This is the dollar amount for the item.  Income, gains, and money received
  are positive numbers.  Expenses, losses, and money spent (including tax
  payments) are negative numbers.

"P" Description
  A string describing this particular line.  This is the value that is
  different and describes the line and therefore should appear on the line
  on the tax form.

"P" Security
  Name of security for schedule D.

"D" Date Acquired
  Acquisition date.  It should be in the form MM/DD/YYYY.

"D" Date Sold
  Sale date.  It should be in the form MM/DD/YYYY.

"D" Date Paid
  Payment date.  It should be in the form MM/DD/YYYY.


Default means that a line does not need to be supplied not that if the
field is supplied with no value the "default" will be used.  So if
NO "C" field is supplied then the default of Copy 1 is to be used.

    */
    
    
    public static final String LF = "\r\n";
    
    private final String recordType = "D";
    private String code = "999";
    private int copy = 1;
    private int line = 1;
    private String description = null;
    private BigDecimal amount = null;
    
    public TxfBean() {
        super();
    }

    public TxfBean( String code, String description ) {
        super();
        this.code = code;
        this.description = description;
    }

    public TxfBean(
        String code,
        BigDecimal amount
    ) {
        super();
        this.code = code;
        this.amount = amount;
    }

    public TxfBean(
        String code,
        int copy,
        int line,
        BigDecimal amount
    ) {
        super();
        this.code = code;
        this.copy = copy;
        this.line = line;
        this.amount = amount;
    }

    public TxfBean(
        String code,
        int copy,
        int line,
        String description
    ) {
        super();
        this.code = code;
        this.copy = copy;
        this.line = line;
        this.description = description;
    }
    
    public TxfBean(
        String code,
        int copy,
        int line,
        String description,
        BigDecimal amount
    ) {
        super();
        this.code = code;
        this.copy = copy;
        this.line = line;
        this.description = description;
        this.amount = amount;
    }

    public String toTxf( ) {
        
        StringWriter writer = new StringWriter( );
        
        // "T" Type
        writer.append( "T" ).append( recordType ).append( LF );
        
        // "N" refnum
        writer.append( "N" ).append( code ).append( LF );
        
        // "C" copy
        writer.append( "C" ).append( Integer.toString( copy ) ).append( LF );
        
        // "L" line
        writer.append( "L" ).append( Integer.toString( line ) ).append( LF );
        
        // "$" Amount
        if ( amount != null ) {
            writer.append( "$" ).append( amount.toString( ) ).append( LF );
        }

        // "P" Description
        if ( ! StringUtils.isEmpty( description ) ) {
            writer.append( "P" ).append( description ).append( LF );
        }

        writer.append( "^" ).append( LF );
        
        return writer.toString( );
        
    }
    
}
