package txf.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TXFRecordFormat5 {
	
	//	For each exchange record there are a set of common fields. This is the
	//	recommended order:
	//	  T type
	//	  N refnum
	//	  C copy
	//	  L line
	//	  X detail	

	//	Record Format 5
	//	  P security
	//	  D date acquired
	//	  D date sold
	//	  $ cost basis
	//	  $ sales net
	//	  $ Depreciation Allowed OR Disallowed wash sale amount (added for TY11)
	//
	//	Note: expanded Format 5 to use for new TY11 cost basis reporting rules. Format 5 will
	//	      be used with Taxrefs 321,323,673,682,711,712,713,714,715,716 when reporting a disallowed wash sale amount. 	
	
	private String refNum;

	private String copy;
	
	private String line;

	private String security;
	
	private String dateAcquired;
	
	private String dateSold;
	
	private String cost;
	
	private String sales;
	
	private String disallowedWashSaleAmount;

	public TXFRecordFormat5( ) {

	}

	public String getRefNum( ) {
		return refNum;
	}

	public void setRefNum( String refNum ) {
		this.refNum = refNum;
	}

	public String getCopy( ) {
		return copy;
	}

	public void setCopy( String copy ) {
		this.copy = copy;
	}

	public String getLine( ) {
		return line;
	}

	public void setLine( String line ) {
		this.line = line;
	}

	public String getSecurity( ) {
		return security;
	}

	public void setSecurity( String security ) {
		this.security = security;
	}

	public String getDateAcquired( ) {
		return dateAcquired;
	}

	public void setDateAcquired( String dateAcquired ) {
		this.dateAcquired = dateAcquired;
	}

	public String getDateSold( ) {
		return dateSold;
	}

	public void setDateSold( String dateSold ) {
		this.dateSold = dateSold;
	}

	public String getCost( ) {
		return cost;
	}

	public void setCost( String cost ) {
		this.cost = cost;
	}

	public String getSales( ) {
		return sales;
	}

	public void setSales( String sales ) {
		this.sales = sales;
	}

	public String getDisallowedWashSaleAmount( ) {
		return disallowedWashSaleAmount;
	}

	public void setDisallowedWashSaleAmount( String disallowedWashSaleAmount ) {
		this.disallowedWashSaleAmount = disallowedWashSaleAmount;
	}

	public String toJson( ) {

		Gson gson = new GsonBuilder( ).setPrettyPrinting( ).create( );

		return gson.toJson( this );

	}

}


