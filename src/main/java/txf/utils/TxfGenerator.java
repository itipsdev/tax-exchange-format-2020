package txf.utils;

import org.apache.commons.lang3.StringUtils;

import txf.models.TxfBean;

import java.math.BigDecimal;

public class TxfGenerator {

    public static String txfText(
        String code,
        int copy,
        String text
    ) {
        
        if ( StringUtils.isEmpty( text ) ) {
            return "";
        }
        
        TxfBean bean = new TxfBean( code, copy, 1, text );
        
        return bean.toTxf( );
        
    }
    
    public static String txfIncome(
        String code,
        int copy,
        BigDecimal amount
    ) {
        
        if ( amount == null ) {
            return "";
        }
        
        if ( amount.compareTo( BigDecimal.ZERO ) == 0 ) {
            return "";
        }
        
        TxfBean bean = new TxfBean( code, copy, 1, amount );
        
        return bean.toTxf( );
        
    }

    public static String txfExpense(
        String code,
        int copy,
        BigDecimal amount
    ) {
        
        if ( amount == null ) {
            return "";
        }
        
        if ( amount.compareTo( BigDecimal.ZERO ) == 0 ) {
            return "";
        }
        
        TxfBean bean = new TxfBean( code, copy, 1, BigDecimal.ZERO.subtract( amount ) );
        
        return bean.toTxf( );
        
    }

    public static String txfExpenseItem(
        String code,
        int copy,
        int lineNumber,
        String description,
        BigDecimal amount
    ) {
        
        if ( amount == null ) {
            return "";
        }
        
        if ( amount.compareTo( BigDecimal.ZERO ) == 0 ) {
            return "";
        }
        
        TxfBean bean = new TxfBean(
            code,
            copy,
            lineNumber,
            description,
            BigDecimal.ZERO.subtract( amount )
        );
        
        return bean.toTxf( );
        
    }
    
}
