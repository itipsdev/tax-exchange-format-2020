package txf.utils;

import txf.models.TXFRecord;
import txf.models.TXFRecordFormat5;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TxfParser {

	// LOGGER

	public static final String CR = "\r";
	public static final String LF = "\n";
	public static final String CARET = "\\^";

    public List<TXFRecord> parse(
    	String txfFileContent
	) {
		
		List<TXFRecord> records = new ArrayList<>( );

		// ===========================================================================
		// Protect against null value
		// ===========================================================================
		if ( txfFileContent == null ) {
			return records;
		}
		
		// TODO: Normalize line endings ?

		// ===========================================================================
		// Break into records
		// ===========================================================================
		String[] txfRecords = txfFileContent.split( CARET );

		int recordCount = txfRecords.length;
		
		// System.out.println( "TXF recordCount: " + recordCount );
		
		for ( String txfRecord : txfRecords ) {
			
			// System.out.println( txfRecord );
			
			TXFRecord record = new TXFRecord( );
			
			// Break into lines
			String[] txfLines = txfRecord.split( "\\n" );
			int lineCount = txfLines.length;
			// System.out.println( "lineCount: " + lineCount );
			
			int count = 0;
			
			for ( String txfLine : txfLines ) {
				
				// System.out.println( txfLine );
				if ( txfLine.length( ) < 2 ) {
					// System.out.println( "Problem record:" + txfLine + ":" );
					continue;
				}
				
				count++;
				
				String firstLetter = txfLine.substring( 0, 1 );

				String remainder   = txfLine.substring( 1 ).trim( );

                switch ( firstLetter ) {

                    case "L":
                        // Extract line number
                        record.setLine( remainder );
                        break;

					case "C":
                        // Extract copy number
                        record.setCopy( remainder );
                        break;

                    case "N":
                        // Extract code aka reference number
                        record.setCode( remainder );
                        break;

                    case "$":
                        // get rid of any commas
                        remainder = remainder.replaceAll( ",", "" );
                        // get rid of decimal portion
                        // TODO: round somehow?
                        remainder = remainder.replaceAll( "\\.\\d\\d", "" );
                        // Extract amount
                        try {
                            record.setAmount( new BigDecimal( remainder ) );
                        } catch ( Exception e ) {
                            System.err.println( e.getMessage( ) );
                            // Bomb out
                            return records;
                        }
                        break;

                    case "P":
                        // Extract description
                        record.setDescription( remainder );
                        break;
                }

				// System.out.println( "" + count + " :" + firstLetter + ": " + remainder );
				
			}

			records.add( record );
			
		}
		
		return records;
		
	}

	@SuppressWarnings("unused")
    public static List<TXFRecordFormat5> parseCapitalGains(
    	String txfFileContent
	) {
		
		List<TXFRecordFormat5> records = new ArrayList<>( );

		// Protect against null value
		if ( txfFileContent == null ) {
			return records;
		}
		
		// TODO: Normalize line endings ?
		// Replace all CR with LF
		txfFileContent = txfFileContent.replaceAll(CR, LF);
		
		// ===========================================================================
		// Get rid of carets that mess up the split
		// ===========================================================================
        txfFileContent = txfFileContent.replaceAll("[\\^]{3}", "---");
        txfFileContent = txfFileContent.replaceAll("[\\^]{2}", "--");

		// ===========================================================================
		// Break into records
		// ===========================================================================
		String[] txfRecords = txfFileContent.split( CARET );
		int recordCount = txfRecords.length;
		if ( recordCount < 2 ) {
			// Problem with split
			System.out.println( "parseCapitalGains: Only " + recordCount + " records" );
			return records;
		}
		
		for ( String txfRecord : txfRecords ) {
			
			// System.out.println( txfRecord );
			
			TXFRecordFormat5 record = new TXFRecordFormat5( );
			
			// Break into lines
			String[] txfLines = txfRecord.split( LF );
			int lineCount = txfLines.length;
			// System.out.println( "lineCount: " + lineCount );
			if ( lineCount < 6 ) {
				// System.out.println( "Discarding txf record: " + txfRecord );
				continue;
			}
			
			int count = 0;
			
			int dateCount = 0;
			int dollarCount = 0;
			
			for ( String txfLine : txfLines ) {
				
				// System.out.println( txfLine );
				if ( txfLine.length( ) < 2 ) {
					// System.out.println( "Problem record:" + txfLine + ":" );
					continue;
				}
				
				count++;
				
				String firstLetter = txfLine.substring( 0, 1 );

				String remainder   = txfLine.substring( 1 ).trim( );

//				switch( firstLetter ) {
//
//					case "":
//						break;
//
//					case "":
//						break;
//
//					case "":
//						break;
//
//					case "":
//						break;
//
//					case "":
//						break;
//
//					case "":
//						break;
//
//
//				}
//
//				if ( "L".equals( firstLetter ) ) {
//					// Extract line number
//					record.line = remainder;
//				}
//				else if ( "N".equals( firstLetter ) ) {
//					// Extract reference number (TXF code)
//					record.refNum = remainder;
//				}
//				else if ( "C".equals( firstLetter ) ) {
//					// Extract copy number
//					record.copy = remainder;
//				}
//				else if ( "D".equals( firstLetter ) ) {
//					// Extract reference number (TXF code)
//					if ( dateCount == 0 ) {
//						dateCount++;
//						record.dateAcquired = remainder;
//					}
//					else if ( dateCount == 1 ) {
//						record.dateSold = remainder;
//					}
//				}
//				else if ( "$".equals( firstLetter ) ) {
//					// Get rid of any commas
//					String dollar = remainder.replace(",", "");
//                    // Get rid of any letters such as USD
//                    dollar = dollar.replaceAll("[a-zA-Z]", "");
//					if ( dollarCount == 0 ) {
//						dollarCount++;
//						record.cost = dollar;
//					}
//					else if ( dollarCount == 1 ) {
//						dollarCount++;
//						record.sales = dollar;
//					}
//					else if ( dollarCount == 2 ) {
//						dollarCount++;
//						record.disallowedWashSaleAmount = dollar;
//					}
//					else {
//						System.err.println( "Too many dollar records found. What's up?" );
//					}
//				}
//				else if ( "P".equals( firstLetter ) ) {
//					// Extract description
//					if (remainder.length() > 50 ) {
//						record.security = remainder.substring(0, 50);
//					}
//					else {
//						record.security = remainder;
//					}
//				}

				// System.out.println( "" + count + " :" + firstLetter + ": " + remainder );
				
			}

			records.add( record );
			
		}
		
		return records;
		
	}

    public static void main( String[] args ) {
        
        String txfFileContent = "Junk ^^^ ^^^ Junk\nJunk ^^^ ^^^ Junk\n";
        
        txfFileContent = txfFileContent.replaceAll("[\\^]{3}", "---");
        
        System.out.println( txfFileContent );
        
    }
    
	
}
