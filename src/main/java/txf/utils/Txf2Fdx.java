package txf.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxdochub.fdx.tax.models.CodeAmount;
import com.taxdochub.fdx.tax.models.NameAddress;
import com.taxdochub.fdx.tax.models.Tax1065K1;
import com.taxdochub.fdx.tax.models.TaxFormAttribute;
import txf.models.TXFRecord;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Txf2Fdx {

    private List<TXFRecord> notHandled = new ArrayList<>( );

    public Txf2Fdx() {

    }

    public List<TXFRecord> getNotHandled() {
        return notHandled;
    }

    public Tax1065K1 convertTxfContentToTax1065K1(
        String txfContent
    ) {

        TxfParser txfParser = new TxfParser( );

        List<TXFRecord> txfRecords = txfParser.parse( txfContent );

        return convertTxfRecordsToTax1065K1( txfRecords );

    }

    public Tax1065K1 convertTxfRecordsToTax1065K1(
        List<TXFRecord> txfRecords
    ) {

        Tax1065K1 tax1065K1 = new Tax1065K1( );

        tax1065K1.setAttributes( new ArrayList<TaxFormAttribute>( ) );

        tax1065K1.setOtherIncome( new ArrayList<CodeAmount>( ) );

        tax1065K1.setOtherDeductions( new ArrayList<CodeAmount>( ) );

        tax1065K1.setSelfEmployment( new ArrayList<CodeAmount>( ) );

        tax1065K1.setCredits( new ArrayList<CodeAmount>( ) );

        tax1065K1.setForeignTransactions( new ArrayList<CodeAmount>( ) );

        tax1065K1.setAmtItems( new ArrayList<CodeAmount>( ) );

        tax1065K1.setTaxExemptIncome( new ArrayList<CodeAmount>( ) );

        tax1065K1.setDistributions( new ArrayList<CodeAmount>( ) );

        tax1065K1.setOtherInfo( new ArrayList<CodeAmount>( ) );

        tax1065K1.setPartnershipNameAddress( new NameAddress( ) );

        tax1065K1.setPartnerNameAddress( new NameAddress( ) );

        for ( TXFRecord txfRecord : txfRecords ) {

            if ( "448".equals( txfRecord.getCode( ) ) ) {

                // Ordinary business income (loss)
                tax1065K1.setOrdinaryIncome( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "449".equals( txfRecord.getCode( ) ) ) {

                // Net rental real estate income (loss)
                tax1065K1.setNetRentalRealEstateIncome( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "450".equals( txfRecord.getCode( ) ) ) {

                // Other net rental income (loss)
                tax1065K1.setOtherRentalIncome( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "451".equals( txfRecord.getCode( ) ) ) {

                // Interest income
                tax1065K1.setInterestIncome( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "452".equals( txfRecord.getCode( ) ) ) {

                // Ordinary dividends
                tax1065K1.setOrdinaryDividends( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "453".equals( txfRecord.getCode( ) ) ) {

                // Net short-term capital gain (loss)
                tax1065K1.setNetShortTermGain( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "454".equals( txfRecord.getCode( ) ) ) {

                // Net long-term capital gain (loss)
                tax1065K1.setNetLongTermGain( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "455".equals( txfRecord.getCode( ) ) ) {

                // Guaranteed payments
                tax1065K1.setGuaranteedPayment( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "456".equals( txfRecord.getCode( ) ) ) {

                // Net section 1231 gain (loss)
                tax1065K1.setNet1231Gain( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "457".equals( txfRecord.getCode( ) ) ) {

                // Partnerships name
                tax1065K1.getPartnershipNameAddress( ).setName1( txfRecord.getDescription( ) );

            } else if ( "527".equals( txfRecord.getCode( ) ) ) {

                // Royalties
                tax1065K1.setRoyalties( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "528".equals( txfRecord.getCode( ) ) ) {

                // Tax-exempt income
                CodeAmount taxExempt = new CodeAmount( );
                taxExempt.setCode( "A" );
                taxExempt.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getTaxExemptIncome( ).add( taxExempt );

            } else if ( "674".equals( txfRecord.getCode( ) ) ) {

                // Collectibles (28%) gain (loss)
                tax1065K1.setCollectiblesGain( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "676".equals( txfRecord.getCode( ) ) ) {

                // Other income (loss)
                CodeAmount otherIncomeLoss = new CodeAmount( );
                otherIncomeLoss.setCode( "F" );
                otherIncomeLoss.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherIncome( ).add( otherIncomeLoss );

            } else if ( "679".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                // Box 16, Foreign transactions. IRS deprecated 2021 and is now reported on Schedule K-3
                this.notHandled.add( txfRecord );

            } else if ( "850".equals( txfRecord.getCode( ) ) ) {

                // Other deductions
                CodeAmount otherDeduction = new CodeAmount( );
                otherDeduction.setCode( "A" );
                otherDeduction.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherDeductions( ).add( otherDeduction );

            } else if ( "851".equals( txfRecord.getCode( ) ) ) {

                // Other deductions
                CodeAmount otherDeduction = new CodeAmount( );
                otherDeduction.setCode( "H" );
                otherDeduction.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherDeductions( ).add( otherDeduction );

            } else if ( "852".equals( txfRecord.getCode( ) ) ) {

                // Other information
                CodeAmount otherInfo = new CodeAmount( );
                otherInfo.setCode( "A" );
                otherInfo.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherInfo( ).add( otherInfo );

            } else if ( "853".equals( txfRecord.getCode( ) ) ) {

                // Other information
                CodeAmount otherInfo = new CodeAmount( );
                otherInfo.setCode( "B" );
                otherInfo.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherInfo( ).add( otherInfo );

            } else if ( "854".equals( txfRecord.getCode( ) ) ) {

                // Alternative minimum tax items
                CodeAmount amtItem = new CodeAmount( );
                amtItem.setCode( "A" );
                amtItem.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getAmtItems( ).add( amtItem );

            } else if ( "855".equals( txfRecord.getCode( ) ) ) {

                // Tax-exempt income
                CodeAmount taxExempt = new CodeAmount( );
                taxExempt.setCode( "C" );
                taxExempt.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getTaxExemptIncome( ).add( taxExempt );

            } else if ( "856".equals( txfRecord.getCode( ) ) ) {

                // Partnerships  Employer Identification Number
                tax1065K1.setPartnershipTin( txfRecord.getDescription( ) );

            } else if ( "857".equals( txfRecord.getCode( ) ) ) {

                // Tax shelter registration number
                this.notHandled.add( txfRecord );

            } else if ( "858".equals( txfRecord.getCode( ) ) ) {

                // Check if publicly traded partnership
                // Boolean type
                tax1065K1.setPubliclyTraded(
                    "X".equals( txfRecord.getDescription( ) ) ||
                    "TRUE".equals( txfRecord.getDescription( ) )
                );

            } else if ( "859".equals( txfRecord.getCode( ) ) ) {

                // Partnerships Name
                tax1065K1.getPartnershipNameAddress( ).setName1( txfRecord.getDescription( ) );

            } else if ( "860".equals( txfRecord.getCode( ) ) ) {

                // Other deductions
                CodeAmount otherDeduction = new CodeAmount( );
                otherDeduction.setCode( "K" /* txfRecord.getDescription( ) */ );
                otherDeduction.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherDeductions( ).add( otherDeduction );

            } else if ( "861".equals( txfRecord.getCode( ) ) ) {

                // Alternative minimum tax items
                CodeAmount amtItem = new CodeAmount( );
                amtItem.setCode( "B" /* txfRecord.getDescription( ) */ );
                amtItem.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getAmtItems( ).add( amtItem );

            } else if ( "862".equals( txfRecord.getCode( ) ) ) {

                // Alternative minimum tax items
                CodeAmount amtItem = new CodeAmount( );
                amtItem.setCode( "C" /* txfRecord.getDescription( ) */ );
                amtItem.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getAmtItems( ).add( amtItem );

            } else if ( "863".equals( txfRecord.getCode( ) ) ) {

                // Alternative minimum tax items
                CodeAmount amtItem = new CodeAmount( );
                amtItem.setCode( "D" /* txfRecord.getDescription( ) */ );
                amtItem.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getAmtItems( ).add( amtItem );

            } else if ( "864".equals( txfRecord.getCode( ) ) ) {

                // Alternative minimum tax items
                CodeAmount amtItem = new CodeAmount( );
                amtItem.setCode( "E" /* txfRecord.getDescription( ) */ );
                amtItem.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getAmtItems( ).add( amtItem );

            } else if ( "865".equals( txfRecord.getCode( ) ) ) {

                // Name of country
                tax1065K1.setForeignCountry( txfRecord.getDescription( ) );

            } else if ( "866".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "867".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "868".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "869".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "870".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "871".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "872".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "873".equals( txfRecord.getCode( ) ) ) {

                // Foreign transactions
                this.notHandled.add( txfRecord );

            } else if ( "882".equals( txfRecord.getCode( ) ) ) {

                // Qualifying dividends
                tax1065K1.setQualifiedDividends( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "889".equals( txfRecord.getCode( ) ) ) {

                // General Partner
                // Boolean type
                tax1065K1.setGeneralPartner( "X".equals( txfRecord.getDescription( ) ) );

            } else if ( "890".equals( txfRecord.getCode( ) ) ) {

                // Limited Partner
                // Boolean type
                tax1065K1.setLimitedPartner( "X".equals( txfRecord.getDescription( ) ) );

            } else if ( "891".equals( txfRecord.getCode( ) ) ) {

                // Beginning capital account
                tax1065K1.setCapitalAccountBegin( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "892".equals( txfRecord.getCode( ) ) ) {

                // Capital contributed during the year
                tax1065K1.setCapitalAccountContributions( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "893".equals( txfRecord.getCode( ) ) ) {

                // Current year increase (decrease)
                tax1065K1.setCapitalAccountIncrease( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "894".equals( txfRecord.getCode( ) ) ) {

                // Withdrawals and distributions
                tax1065K1.setCapitalAccountWithdrawals( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "895".equals( txfRecord.getCode( ) ) ) {

                // Ending capital account
                tax1065K1.setCapitalAccountEnd( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "896".equals( txfRecord.getCode( ) ) ) {

                // Other Income
                CodeAmount otherIncomeLoss = new CodeAmount( );
                otherIncomeLoss.setCode( txfRecord.getDescription( ) );
                otherIncomeLoss.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherIncome( ).add( otherIncomeLoss );

            } else if ( "897".equals( txfRecord.getCode( ) ) ) {

                // Other Deductions
                CodeAmount otherDeduction = new CodeAmount( );
                otherDeduction.setCode( txfRecord.getDescription( ) );
                otherDeduction.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherDeductions( ).add( otherDeduction );

            } else if ( "898".equals( txfRecord.getCode( ) ) ) {

                // Self-employment earnings (loss)
                CodeAmount selfEmploy = new CodeAmount( );
                selfEmploy.setCode( txfRecord.getDescription( ) );
                selfEmploy.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getSelfEmployment( ).add( selfEmploy );

            } else if ( "899".equals( txfRecord.getCode( ) ) ) {

                // Credits
                CodeAmount credits = new CodeAmount( );
                credits.setCode( txfRecord.getDescription( ) );
                credits.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getCredits( ).add( credits );

            } else if ( "900".equals( txfRecord.getCode( ) ) ) {

                // Foreign Transactions
                this.notHandled.add( txfRecord );

            } else if ( "901".equals( txfRecord.getCode( ) ) ) {

                // Alternative minimum tax items
                CodeAmount amtItem = new CodeAmount( );
                amtItem.setCode( txfRecord.getDescription( ) );
                amtItem.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getAmtItems( ).add( amtItem );

            } else if ( "902".equals( txfRecord.getCode( ) ) ) {

                // Tax-exempt interest income
                CodeAmount taxExempt = new CodeAmount( );
                taxExempt.setCode( txfRecord.getDescription( ) );
                taxExempt.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getTaxExemptIncome( ).add( taxExempt );

            } else if ( "903".equals( txfRecord.getCode( ) ) ) {

                // Distributions
                CodeAmount distribution = new CodeAmount( );
                distribution.setCode( txfRecord.getDescription( ) );
                distribution.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getDistributions( ).add( distribution );

            } else if ( "904".equals( txfRecord.getCode( ) ) ) {

                // Other information
                CodeAmount otherInfo = new CodeAmount( );
                otherInfo.setCode( txfRecord.getDescription( ) );
                otherInfo.setAmount( toDouble( txfRecord.getAmount( ) ) );
                tax1065K1.getOtherInfo( ).add( otherInfo );

            } else if ( "907".equals( txfRecord.getCode( ) ) ) {

                // Section 179 deduction
                tax1065K1.setSection179Deduction( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "908".equals( txfRecord.getCode( ) ) ) {

                // Unrecaptured section 1250 gain
                tax1065K1.setUnrecaptured1250Gain( toDouble( txfRecord.getAmount( ) ) );

            } else if ( "913".equals( txfRecord.getCode( ) ) ) {

                // Check if materially participated
                // Boolean type
                // tax1065K1.setMateriallyParticipated( "X".equals(txfRecord.getDescription( )) ? BooleanType.Y : BooleanType.N );
                this.notHandled.add( txfRecord );

            } else if ( "914".equals( txfRecord.getCode( ) ) ) {

                // Partnership address
                tax1065K1.getPartnershipNameAddress( ).setLine1( txfRecord.getDescription( ) );

            } else if ( "915".equals( txfRecord.getCode( ) ) ) {

                // Partnership city
                tax1065K1.getPartnershipNameAddress( ).setCity( txfRecord.getDescription( ) );

            } else if ( "916".equals( txfRecord.getCode( ) ) ) {

                // Partnership state
                tax1065K1.getPartnershipNameAddress( ).setState( txfRecord.getDescription( ) );

            } else if ( "917".equals( txfRecord.getCode( ) ) ) {

                // Partnership ZIP code
                tax1065K1.getPartnershipNameAddress( ).setPostalCode( txfRecord.getDescription( ) );

            } else if ( "918".equals( txfRecord.getCode( ) ) ) {

                // Income or loss from REMIC
                // tax1065K1.setREMICIncome( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "919".equals( txfRecord.getCode( ) ) ) {

                // Excess inclusion from REMIC
                // tax1065K1.setREMICExcessInclusion( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "920".equals( txfRecord.getCode( ) ) ) {

                // Section 212 expense from REMIC
                // tax1065K1.setREMICSection121( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "921".equals( txfRecord.getCode( ) ) ) {

                // Other portfolio income reported on Schedule E
                // tax1065K1.setOtherPortfolioScheduleEDesc( txfRecord.getDescription( ) );
                // tax1065K1.setOtherPortfolioScheduleEAmount( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "922".equals( txfRecord.getCode( ) ) ) {

                // Recoveries
                // tax1065K1.setRecoveries( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "923".equals( txfRecord.getCode( ) ) ) {

                // Ordinary gain (loss) reported on Form 4797
                // tax1065K1.setOrdinaryGain4797( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "924".equals( txfRecord.getCode( ) ) ) {

                // Nonportfolio short-term capital gain (loss)
                // tax1065K1.setNonPortfolioSTGain( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "925".equals( txfRecord.getCode( ) ) ) {

                // Nonportfolio long-term capital gain (loss)
                // tax1065K1.setNonPortfolioLTGain( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "926".equals( txfRecord.getCode( ) ) ) {

                // Other nonpassive income reported on Schedule E
                // tax1065K1.setOtherIncomeScheduleEDesc( txfRecord.getDescription( ) );
                // tax1065K1.setOtherIncomeScheduleEAmount( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "927".equals( txfRecord.getCode( ) ) ) {

                // Section 59(e)(2) expenditure description
                // tax1065K1.setSection59E2Desc( txfRecord.getDescription( ) );
                this.notHandled.add( txfRecord );

            } else if ( "928".equals( txfRecord.getCode( ) ) ) {

                // Other taxes
                // tax1065K1.setOtherTaxes( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "929".equals( txfRecord.getCode( ) ) ) {

                // Other miscellaneous itemized deductions
                // tax1065K1.setOtherMiscItemized( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "930".equals( txfRecord.getCode( ) ) ) {

                // Section 754 depreciation
                // tax1065K1.setSection754Depreciation( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "931".equals( txfRecord.getCode( ) ) ) {

                // State income tax withheld
                // StateIncomeTaxWithheld stateIncomeTaxWithheld = new StateIncomeTaxWithheld( );
                // stateIncomeTaxWithheld.setStateID( txfRecord.getDescription( ) );
                // stateIncomeTaxWithheld.setAmount( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "932".equals( txfRecord.getCode( ) ) ) {

                // Other nonpassive deduction reported on Schedule E
                // tax1065K1.setOtherDeductionScheduleEDesc( txfRecord.getDescription( ) );
                // tax1065K1.setOtherDeductionScheduleEAmount( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "933".equals( txfRecord.getCode( ) ) ) {

                // Investment interest expense reported on Schedule E
                // tax1065K1.setInvestmentInterestExpenseScheduleE( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "934".equals( txfRecord.getCode( ) ) ) {

                // Elect to claim foreign tax credit
                // Boolean type
                // tax1065K1.setClaimForeignTaxCredit( "X".equals(txfRecord.getDescription( )) ? BooleanType.Y : BooleanType.N );
                this.notHandled.add( txfRecord );

            } else if ( "935".equals( txfRecord.getCode( ) ) ) {

                // Elect to claim foreign tax deduction
                // Boolean type
                // tax1065K1.setClaimForeignTaxDeduction( "X".equals(txfRecord.getDescription( )) ? BooleanType.Y : BooleanType.N );
                this.notHandled.add( txfRecord );

            } else if ( "936".equals( txfRecord.getCode( ) ) ) {

                // Elect to forego completion of Form 1116
                // Boolean type
                // tax1065K1.setElectForegoForm1116( "X".equals(txfRecord.getDescription( )) ? BooleanType.Y : BooleanType.N );
                this.notHandled.add( txfRecord );

            } else if ( "937".equals( txfRecord.getCode( ) ) ) {

                // Adjusted gain or loss - Section 1231
                // tax1065K1.setAMTAdjustSection1231Gain( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "938".equals( txfRecord.getCode( ) ) ) {

                // Adjusted gain or loss - Section 1250
                // tax1065K1.setAMTAdjustSection1250Gain( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "939".equals( txfRecord.getCode( ) ) ) {

                // Excess intangible drilling costs
                // tax1065K1.setExcessIDC( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "940".equals( txfRecord.getCode( ) ) ) {

                // Excess depletion
                // tax1065K1.setExcessDepletion( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "941".equals( txfRecord.getCode( ) ) ) {

                // Tax exempt interest
                //                TaxExemptInterestIncomeDetail taxExemptInterestIncomeDetail = new TaxExemptInterestIncomeDetail( );
                //                taxExemptInterestIncomeDetail.setStateID( txfRecord.getDescription( ) );
                //                taxExemptInterestIncomeDetail.setAmount( txfRecord.getAmount( ) );
                //                tax1065K1.getTaxExemptInterestIncomeDetail( ).add( taxExemptInterestIncomeDetail );

                this.notHandled.add( txfRecord );


            } else if ( "943".equals( txfRecord.getCode( ) ) ) {

                // Partners name
                tax1065K1.getPartnerNameAddress( ).setName1( txfRecord.getDescription( ) );

            } else if ( "944".equals( txfRecord.getCode( ) ) ) {

                // Partners identifying number
                tax1065K1.setPartnerTin( txfRecord.getDescription( ) );

            } else if ( "945".equals( txfRecord.getCode( ) ) ) {

                // Depletion expense
                // tax1065K1.setDepletionExpense( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "947".equals( txfRecord.getCode( ) ) ) {

                // Cost depletion
                // tax1065K1.setCostDepletion( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "948".equals( txfRecord.getCode( ) ) ) {

                // Gross income from oil and gas
                // tax1065K1.setPercentDepletionGrossIncome( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else if ( "949".equals( txfRecord.getCode( ) ) ) {

                // Percentage depletion rate
                // RateType accurate? Not accounted for.  String works?
//                if ( txfRecord.getAmount( ) != null ) {
//                    tax1065K1.setPercentDepletionRate( txfRecord.getAmount( ).toString( ) );
//                }
                this.notHandled.add( txfRecord );

            } else if ( "950".equals( txfRecord.getCode( ) ) ) {

                // Percentage depletion amount
                // tax1065K1.setPercentDepletionAmount( txfRecord.getAmount( ) );
                this.notHandled.add( txfRecord );

            } else {

                this.notHandled.add( txfRecord );

            }

        }

        return tax1065K1;

    }

    public static Double toDouble(
        BigDecimal bigDecimal
    ) {

        if ( bigDecimal == null ) return Double.valueOf( "0" );

        return bigDecimal.doubleValue( );

    }

    public static void main(String[] args) {

        String path = "/Users/brucewilcox/dev/taxdochub2022/tax-exchange-format-2020/inputs/k1.1.txf";

        String content = "V042\n" +
            "AK-1 Mgr\n" +
            "D07/20/2021\n" +
            "^\n" +
            "TS\n" +
            "N447\n" +
            "C1\n" +
            "L1\n" +
            "PX\n" +
            "^\n" +
            "TS\n" +
            "N457\n" +
            "C1\n" +
            "L1\n" +
            "PPartnership name\n" +
            "^\n" +
            "TS\n" +
            "N446\n" +
            "C1\n" +
            "L1\n" +
            "PSchedule K-1 Worksheet\n" +
            "$446.00\n" +
            "^\n" +
            "TS\n" +
            "N448\n" +
            "C1\n" +
            "L1\n" +
            "POrdinary income/loss\n" +
            "$448.00\n" +
            "^\n" +
            "TS\n" +
            "N449\n" +
            "C1\n" +
            "L1\n" +
            "PRental real est. inc/loss\n" +
            "$449.00\n" +
            "^\n" +
            "TS\n" +
            "N450\n" +
            "C1\n" +
            "L1\n" +
            "POther rental income/loss\n" +
            "$450.00\n" +
            "^\n" +
            "TS\n" +
            "N451\n" +
            "C1\n" +
            "L1\n" +
            "PInterest income\n" +
            "$451.00\n" +
            "^\n" +
            "TS\n" +
            "N452\n" +
            "C1\n" +
            "L1\n" +
            "PDividends\n" +
            "$452.00\n" +
            "^\n" +
            "TS\n" +
            "N453\n" +
            "C1\n" +
            "L1\n" +
            "PNet ST capital gain/loss\n" +
            "$453.00\n" +
            "^\n" +
            "TS\n" +
            "N454\n" +
            "C1\n" +
            "L1\n" +
            "PNet LT capital gain/loss\n" +
            "$454.00\n" +
            "^\n" +
            "TS\n" +
            "N455\n" +
            "C1\n" +
            "L1\n" +
            "PGuaranteed payments\n" +
            "$455.00\n" +
            "^\n" +
            "TS\n" +
            "N456\n" +
            "C1\n" +
            "L1\n" +
            "PNet sec 1231 gain/loss\n" +
            "$456.00\n" +
            "^\n" +
            "TS\n" +
            "N527\n" +
            "C1\n" +
            "L1\n" +
            "PRoyalties\n" +
            "$527.00\n" +
            "^\n" +
            "TS\n" +
            "N528\n" +
            "C1\n" +
            "L1\n" +
            "PTax-exempt interest\n" +
            "$528.00\n" +
            "^\n" +
            "TS\n" +
            "N674\n" +
            "C1\n" +
            "L1\n" +
            "P28% rate gain(loss)\n" +
            "$674.00\n" +
            "^\n" +
            "TS\n" +
            "N675\n" +
            "C1\n" +
            "L1\n" +
            "PQualified 5-year gain\n" +
            "$675.00\n" +
            "^\n" +
            "TS\n" +
            "N676\n" +
            "C1\n" +
            "L1\n" +
            "POther Income (loss)\n" +
            "$676.00\n" +
            "^\n" +
            "TS\n" +
            "N679\n" +
            "C1\n" +
            "L1\n" +
            "PTotal Foreign Taxes\n" +
            "$-679.00\n" +
            "^";

        Txf2Fdx txf2Fdx = new Txf2Fdx( );

        Tax1065K1 tax1065K1 = txf2Fdx.convertTxfContentToTax1065K1( content );

        Gson gson = new GsonBuilder( ).setPrettyPrinting( ).create( );

        String json = gson.toJson( tax1065K1 );

        System.out.println(
            json
        );

    }

}
